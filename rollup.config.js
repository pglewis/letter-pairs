import includePaths from "rollup-plugin-includepaths";
import nodeResolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import handlebars from "rollup-plugin-handlebars-plus";
import pug from "rollup-plugin-pug";
import babel from "rollup-plugin-babel";
import { uglify } from "rollup-plugin-uglify";

const paths = {
	scriptSourcesRoot: "site/client/scripts",
	scriptBundleRoot: "site/public/scripts",

	get entry () {
		return `${this.scriptSourcesRoot}/app-boot.js`;
	},

	get output () {
		return `${this.scriptBundleRoot}/app.min.js`;
	},

	get hbsHelpers () {
		return `util/handlebars-helpers.js`;
	}
};

const includePathOptions = {
	include: {},
	paths: [ paths.scriptSourcesRoot ],
	external: [],
	extensions: [ ".js", ".pug", ".hbs" ]
};

export default {
	input: paths.entry,
	output: {
		file: paths.output,
		format: "iife",
		name: "App", // One single object added to the global namespace
		external: [
			"jQuery",
			"_",
			"Backbone",
			"Marionette",
			"Handlebars"
		],
		globals: {
			"handlebars/runtime.js": "Handlebars"
		},
		sourcemap: true
	},
	plugins: [
		nodeResolve(),
		commonjs( {
			include: "node_modules/**"
		} ),
		includePaths( includePathOptions ),
		handlebars( {
			helpers: [ paths.hbsHelpers ]
		} ),
		pug(),
		babel( {
			babelrc: false,
			presets: [ [ "env", { modules: false } ] ],
			plugins: [ "external-helpers" ]
		} ),
		uglify()
	]
};
