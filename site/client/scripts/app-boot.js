import { LetterPairsApplication } from "letter-pairs-application";
import { ImageModel, ImageCollection } from "models/image-model";
import { LetterPairModel } from "models/letter-pair-model";

const App = {
	LetterPairModel: LetterPairModel,
	ImageModel: ImageModel,
	ImageCollection: ImageCollection
};

// Boot the app when the DOM is ready
document.addEventListener( "DOMContentLoaded", () => {
	const AppInstance = new LetterPairsApplication( { region: "body" } );
	AppInstance.start();
	App.AppInstance = AppInstance;
} );

// noinspection JSUnusedGlobalSymbols
export default App;
