import { RootLayout } from "root-layout";

export const LetterPairsApplication = Marionette.Application.extend( {
	onStart: function () {
		this.showView( new RootLayout() );
	}
} );

