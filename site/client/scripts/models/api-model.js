// Custom parse function for the API's wrapper scheme
const customParse = ( response ) => {
	if ( response.data ) {
		return response.data;
	} else {
		return response;
	}
};

export const APIModel = Backbone.RelationalModel.extend( {
	parse: customParse
} );

export const APICollection = Backbone.Collection.extend( {
	parse: customParse
} );
