export const ColorSchemeModel = Backbone.Model.extend( {
	// Default to WCA scramble orientation with standard color scheme
	defaults: {
		uFace: "#FFFFFF",
		lFace: "#FFA500",
		fFace: "#008000",
		rFace: "#FF0000",
		bFace: "#0058ff",
		dFace: "#FFFF00"
	}
} );
