import { APIModel, APICollection } from "models/api-model";
import apiUrlTable from "util/api-url-table";

export const ImageModel = APIModel.extend( {
	urlRoot: apiUrlTable.images,
	idAttribute: "imageID",

	defaults: {
		imageID: null,
		letterPairID: null,
		image: null,
		isPrivate: false
	}
} );

export const ImageCollection = APICollection.extend( {
	url: apiUrlTable.images,
	model: ImageModel
} );
