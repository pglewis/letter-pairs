import { APIModel } from "models/api-model";
import { ImageModel, ImageCollection } from "models/image-model";
import apiUrlTable from "util/api-url-table";

export const LetterPairModel = APIModel.extend( {
	urlRoot: apiUrlTable.pairs,
	idAttribute: "letterPair",

	defaults: {
		letterPair: null,
		letterPairID: null
	},

	relations: [ {
		type: Backbone.HasMany,
		key: "images",
		relatedModel: ImageModel,
		collectionType: ImageCollection,
		reverseRelations: {
			key: "letterPair"
		}
	} ]
} );
