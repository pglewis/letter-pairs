import { APIModel, APICollection } from "models/api-model";
import apiUrlTable from "util/api-url-table";

export const UserModel = APIModel.extend( {
	urlRoot: apiUrlTable.users,
	defaults: {
		id: null,
		name: "",
		config: ""
	}
} );

export const UserCollection = APICollection.extend( {
	url: apiUrlTable.users,
	model: UserModel
} );
