import template from "root-layout.pug";
import { NavView } from "views/nav/nav-view";
import { FlashcardsView } from "views/flashcards/flashcards-view";
import { FindImagesView } from "views/find-images/find-images-view";

export const RootLayout = Marionette.View.extend( {
	id: "#root-layout",

	regions: {
		nav: "#nav",
		app: "#app"
	},

	template: template,

	onRender: function () {
		this.showChildView( "nav", new NavView() );
		this.showChildView( "app", new FindImagesView() );
	}
} );
