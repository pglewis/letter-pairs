export default {
	users: "/api/users",
	pairs: "/api/pairs",
	images: "/api/images"
};
