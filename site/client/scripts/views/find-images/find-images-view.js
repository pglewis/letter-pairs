/*@global jQuery, Backbone, _, Marionette */
import template from "./find-images-view.pug";

import { LetterPairModel } from "models/letter-pair-model";
import { ImageModel } from "models/image-model";
import { ImageCollectionView } from "views/images/image-view";
import { NewImageView } from "views/new-image/new-image-view";

export const FindImagesView = Marionette.View.extend( {
	template: template,

	ui: {
		textInput: "#text-input",
		searchButton: "#search-button",

		// @todo: rework logic to only need one element and event to handle these two
		// (or better, create a view for it)
		firstLetterCell: "#first-letter .lg-cell",
		secondLetterCell: "#second-letter .lg-cell",

		imageResultsContainer: "#image-results-container",
		imagesCollectionRegion: "#images-collection-region",
		openAddNewButton: "#image-results-container .open-add-new",

		addNewContainer: "#add-new-container",
		addNewViewRegion: "#add-new-view-region",
		addNewImageButton: "#add-new-container .add-new-image"
	},

	regions: {
		imagesCollectionRegion: "@ui.imagesCollectionRegion",
		addNewViewRegion: "@ui.addNewViewRegion"
	},

	triggers: {
		"click @ui.searchButton": "search:button:click",
		"keyup @ui.textInput": "text:input:keyup",
		"click @ui.firstLetterCell": "first:letter:click",
		"click @ui.secondLetterCell": "second:letter:click",

		"hidden.bs.modal @ui.imageResultsContainer": "results:hidden",
		"click @ui.openAddNewButton": "open:add:new:button:click",

		"hidden.bs.modal @ui.addNewContainer": "add:new:hidden",
		"click @ui.addNewImageButton": "add:new:image:button:click"
	},

	modelEvents: {
		sync: "render",
		error: "onModelError"
	},

	firstLetter: false,
	secondLetter: false,

	initialize: function () {
		this.model = new LetterPairModel();
	},

	onRender: function () {
		this.firstLetter = false;
		this.secondLetter = false;

		if ( this.model.get( "letterPair" ) ) {
			const newCollectionView = new ImageCollectionView( {
				collection: this.model.get( "images" )
			} );

			this.showChildView( "imagesCollectionRegion", newCollectionView );
			this.showModal( "imageResultsContainer" );
		}
	},

	onAttach: function () {
		this.getUI( "textInput" ).focus();
	},

	/**
	 * Model fetch
	 */
	fetchImages: function ( pair ) {
		this.model.clear( { silent: true } );
		this.model.set( "letterPair", pair.toLowerCase() );
		this.model.fetch();
	},

	onModelError: function ( model, response, options ) {
		// API returns 404 if no matching pair was found
		if ( 404 === response.status ) {
			// @todo: act
			this.render();
		} else {
			console.error( "Fetch Failed" );
			console.log( model );
			console.log( response );
			console.log( options );
		}
	},

	/**
	 * Modal helper methods and event listeners
	 */
	showModal: function ( regionName ) {
		// noinspection JSUnresolvedFunction
		this.getUI( regionName ).modal();
	},

	hideModal: function ( regionName ) {
		// noinspection JSUnresolvedFunction
		this.getUI( regionName ).modal( "hide" );
	},

	// Results Events
	onOpenAddNewButtonClick: function () {
		this.hideModal( "imageResultsContainer" );
		this.showAddNew();
	},

	onResultsHidden: function () {
		this.getUI( "textInput" ).focus();
	},

	/**
	 * Add new
	 */
	showAddNew: function () {
		const newModel = new ImageModel( {
			letterPair: this.model.get( "letterPair" ),
			letterPairID: this.model.get( "letterPairID" )
		} );

		this.showChildView(
			"addNewViewRegion",
			new NewImageView( { model: newModel } )
		);
		this.showModal( "addNewContainer" );
	},

	onAddNewHidden: function () {
		this.showModal( "imageResultsContainer" );
	},

	onAddNewImageButtonClick: function () {
		const imageCollection = this.model.get( "images" );
		const newModel = this.getChildView( "addNewViewRegion" ).model;
		const imageText = this.getChildView( "addNewViewRegion" ).getUI( "newImageText" ).val();

		if ( imageText.length ) {
			newModel.set( "image", imageText );
			newModel.save( null, {
				success: () => {
					imageCollection.add( newModel );
					this.hideModal( "addNewContainer" );
				},
				error: ( model, response, options ) => {
					console.error( "Error creating new image:" );
					console.error( `${response.status} ${response.responseText}` );
					this.hideModal( "addNewContainer" );
				}
			} );
		}
	},

	/**
	 * Text search
	 */
	onSearchButtonClick: function () {
		this.fetchImages( this.getUI( "textInput" ).val() );
	},

	onTextInputKeyup: function ( view, event ) {
		if ( "Enter" === event.key ) {
			this.fetchImages( this.getUI( "textInput" ).val() );
		}
	},

	/**
	 * Letter grid search
	 */
	onFirstLetterClick: function ( view, event ) {
		const $target = jQuery( event.target );
		this.firstLetter = $target.text();
		this.letterSelected( $target );
	},

	onSecondLetterClick: function ( view, event ) {
		const $target = jQuery( event.target );
		this.secondLetter = jQuery( event.target ).text();
		this.letterSelected( $target );
	},

	letterSelected: function ( $target ) {

		// Clear any previously selected cell in this grid
		$target.parents( ".letter-grid" )
		.find( ".selected" )
		.removeClass( "selected" );

		$target.addClass( "selected" );

		if ( this.firstLetter && this.secondLetter ) {
			this.fetchImages( this.firstLetter + this.secondLetter );
		}
	}
} );
