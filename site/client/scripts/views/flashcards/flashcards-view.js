import template from "./flashcards-view.pug";
import { LetterPairModel } from "models/letter-pair-model";
import { ImageCollectionView } from "views/images/image-view";

export const FlashcardsView = Marionette.View.extend( {
	template: template,

	regions: {
		images: "#images"
	},

	ui: {
		pair: "#pair",
		showButton: "button.show-images",
		nextButton: "button.next"
	},

	triggers: {
		"click @ui.showButton": "show:button:click",
		"click @ui.nextButton": "next:button:click"
	},

	modelEvents: {
		sync: "render"
	},

	answerShown: false,

	templateContext: function () {
		return {
			answerShown: this.answerShown
		};
	},

	initialize: function () {
		this.model = new LetterPairModel();
		this.fetchRandomPair();
	},

	onRender: function () {
		if ( this.answerShown ) {
			const collection = this.model.get( "images" );
			this.showChildView(
				"images",
				new ImageCollectionView( { collection: collection } )
			);
		}

		this.getUI( this.answerShown ? "nextButton" : "showButton" ).focus();
	},

	fetchRandomPair: function () {
		// 'random' is a pairs endpoint that will redirect to a specific pair
		this.model.set( "letterPair", "random" );
		this.model.fetch();
	},

	/**
	 * Button event handlers
	 */
	onShowButtonClick: function () {
		this.answerShown = true;
		this.render();
	},

	onNextButtonClick: function () {
		this.answerShown = false;
		this.model.clear( { silent: true } );
		this.fetchRandomPair();
	}
} );
