import itemTemplate from "./image-view.pug";

/**
 *
 */
const EmptyImageView = Marionette.View.extend( {
	className: "p-4",
	attributes: {
		"style": "font-size: 125%;"
	},
	template: _.template( "No images exist for this letter pair." )
} );

/**
 *
 */
export const ImageView = Marionette.View.extend( {
	tagName: "button",

	className: function () {
		return "btn btn-secondary btn-sm my-1 ml-1" +
			( !this.model.get( "isPrivate" ) ? " private" : "" );
	},

	template: itemTemplate,

	ui: {
		deleteButton: ".delete-image",
		isPrivate: ".is-private-checkbox"
	},

	triggers: {
		"click @ui.isPrivate": "toggle:is:private",
		"click @ui.deleteButton": "delete:button:click"
	},

	templateContext: function () {
		return {
			uniqueID: this.cid
		};
	},

	onToggleIsPrivate: function () {
		this.model.set( "isPrivate", !this.model.get( "isPrivate" ) );
	},

	onDeleteButtonClick: function () {

		// @Todo: Needs confirmation
		this.model.destroy();
	}
} );

/**
 *
 */
export const ImageCollectionView = Marionette.CollectionView.extend( {
	id: "image-collection",

	emptyView: EmptyImageView,
	childView: ImageView,

	collectionEvents: {
		"sync": "render"
	}
} );
