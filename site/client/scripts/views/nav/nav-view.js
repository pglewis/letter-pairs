import template from "./nav-view.hbs";

export const NavView = Marionette.View.extend( {
	template: template
} );
