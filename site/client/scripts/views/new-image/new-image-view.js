import template from "./new-image-view.hbs";
import { ImageModel } from "models/image-model";

export const NewImageView = Marionette.View.extend( {
	template: template,

	ui: {
		newImageText: "#new-image-text"
	},

	onRender: function () {
		this.getUI( "newImageText" ).focus();
	},

	onAttach: function () {
		this.getUI( "newImageText" ).focus();
	}
} );
