import template from "./users-app.pug";
import { UserModel, UserCollection } from "models/user-model";

const UserListItem = Marionette.View.extend( {
	tagName: "li",
	className: "list-group-item text-light bg-primary clickable",
	template: template,

	ui: {
		user: ".name"
	},
	triggers: {
		"click @ui.user": "user:click"
	},

	onUserClick: function () {
		console.dir( this.model );
	}
} );

const UserListView = Marionette.CollectionView.extend( {
	tagName: "ul",
	className: "list-group",
	childView: UserListItem
} );

const users = new UserCollection();
const listView = new UserListView( { collection: users } );

listView.render();
jQuery( "#app" ).append( listView.$el );
users.fetch();

export default {
	listView: listView,
	users: users,
	UserModel: UserModel,
	UserCollection: UserCollection
};
