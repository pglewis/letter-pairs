const createError = require( "../../utils/errors" );
const controller = {};

// All images
controller.getAll = ( req, res, next ) => {
	const sql = `
		SELECT id AS imageID,
			letter_pair_id AS letterPairID,
			image,
			is_private AS isPrivate
		FROM images
	`;

	req.getConnection( ( err, conn ) => {
		conn.query( sql, ( err, results ) => {
			if ( err ) {
				return next( createError( 500, err ) );
			}

			res.status( 200 ).json( results );
		} );
	} );
};

// Single image
controller.getByID = ( req, res, next ) => {
	const id = req.params.id;
	const sql = `
		SELECT id AS imageID,
			image,
			is_private AS isPrivate
		FROM images
		WHERE id = ?
	`;

	req.getConnection( ( err, conn ) => {
		conn.query( sql, [ id ], ( err, results ) => {
			if ( err ) {
				return next( createError( 500, err ) );
			}

			// Reject with a 404 if nothing was found
			if ( !results.length ) {
				return res.status( 404).send( `Image ID ${id} not found` );
			}

			res.status( 200 ).json( { data: results[ 0 ] } );
		} );
	} );
};

// Add New
controller.add = ( req, res, next ) => {
	const letterPairID = req.body.letterPairID;
	const image = req.body.image;
	const isPrivate = req.body.isPrivate;

	// @todo: user ID is currently hardcoded
	const sql = `
		INSERT INTO images(user_id, letter_pair_id, image, is_private)
		VALUES (1, ?, ?, ?)
	`;

	req.getConnection( ( err, conn ) => {
		conn.query( sql, [ letterPairID, image, isPrivate ], ( err, results ) => {
			if ( err ) {
				return next( createError( 500, err ) );
			}

			// 303 SEE OTHER
			// Success, redirect to the newly inserted record
			res.redirect( 303, `images/${results.insertId}` );
		} );
	} );
};

// Delete
controller.delete = ( req, res, next ) => {
	const id = req.params.id;
	const sql = `
		DELETE
		FROM images
		WHERE id = ?
	`;

	req.getConnection( ( err, conn ) => {
		conn.query( sql, [ id ], ( err, results ) => {
			if ( err ) {
				return next( createError( 500, err ) );
			}

			// noinspection JSUnresolvedVariable
			if ( results.affectedRows === 0 ) {
				return next( createError(
					404,
					`Image ID ${id} does not exist.`
				) );
			} else {
				// 204 NO CONTENT
				// The server has successfully fulfilled the request and
				// there is no additional content to send in the response
				// payload body.
				res.status( 204 ).send();
			}
		} );
	} );
};

module.exports = controller;
