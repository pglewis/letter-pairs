const createError = require( "../../utils/errors" );
const controller = {};

// All letter pairs
controller.allPairs = ( req, res, next ) => {
	const sql = `
		SELECT 
			id AS letterPairID, 
			letter_pair AS letterPair
		FROM letter_pairs 
		ORDER BY letter_pair
	`;

	req.getConnection( ( err, conn ) => {
		conn.query( sql, ( err, results ) => {
			if ( err ) {
				return next( createError( 500, err ) );
			}

			res.status( 200 ).json( results );
		} );
	} );
};

// Single letter pair
controller.getByLetterPair = ( req, res, next ) => {
	const letterPair = req.params.letterPair;
	const sql = `
		SELECT 
			lp.id AS letterPairID,
			lp.letter_pair as letterPair,
			i.id as imageID, 
			i.image, 
			i.is_private AS isPrivate 
		FROM letter_pairs AS lp 
		LEFT JOIN images AS i 
			ON lp.id = i.letter_pair_id
		WHERE lp.letter_pair = ?
	`;

	req.getConnection( ( err, conn ) => {
		conn.query( sql, [ letterPair ], ( err, results ) => {
			const data = {};

			if ( err ) {
				return next( createError( 500, err ) );
			}

			// Reject with a 404 if nothing was found
			if ( !results.length ) {
				return next( createError( 404, `Letter pair '${letterPair}' not found` ) );
			}

			data.letterPairID = results[ 0 ].letterPairID;
			data.letterPair = letterPair;
			data.images = [];

			// Any images found for the letter pair?
			if ( results[ 0 ].imageID !== null ) {

				// Extract only the object fields that belong in the images model
				data.images = results.map( element => {
					return {
						imageID: element.imageID,
						letterPairID: element.letterPairID,
						image: element.image,
						isPrivate: element.isPrivate
					};
				} );
			}

			res.status( 200 ).json( { data: data } );
		} );
	} );
};

// Random pair
controller.randomPair = ( req, res, next ) => {
	const sql = `
		SELECT letter_pair as letterPair 
		FROM letter_pairs
		ORDER BY RAND() 
		LIMIT 1  
	`;

	req.getConnection( ( err, conn ) => {
		conn.query( sql, ( err, results ) => {
			if ( err ) {
				return next( createError( 500, err ) );
			}
			res.redirect( 303, `${results[ 0 ].letterPair.toLocaleLowerCase()}` );
		} );
	} );
};

module.exports = controller;
