const createError = require( "../../utils/errors" );
const controller = {};

// All users
controller.getAll = ( req, res, next ) => {
	const sql = `SELECT id, name, config FROM users`;

	req.getConnection( ( err, conn ) => {
		conn.query( sql, ( err, results ) => {
			if ( err ) {
				return next( createError( 500, err ) );
			}
			res.status( 200 ).json( { data: results } );
		} );
	} );
};

// Specified user
controller.getByID = ( req, res, next ) => {
	const userID = req.params.userID;
	const sql = `SELECT id, name, config FROM users WHERE id = ?`;

	req.getConnection( ( err, conn ) => {
		conn.query( sql, [ userID ], ( err, results ) => {
			if ( err ) {
				return next( createError( 500, err ) );
			}

			// Reject with a 404 if nothing was found (use images/search/:letterPair to get an empty
			// object when nothing is found
			if ( !results.length ) {
				return next( createError( 404, `User ID ${userID} not found` ) );
			}

			res.status( 200 ).json( { data: results[ 0 ] } );
		} );
	} );
};

module.exports = controller;
