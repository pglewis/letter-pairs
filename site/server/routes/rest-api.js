const router = require( "express" ).Router();
const createError = require( "../utils/errors" );

const pairs = require( "./controllers/letter-pairs-controller" );
const images = require( "./controllers/images-controller" );
const users = require( "./controllers/users-controller" );

// Database
router.use( require( "../utils/db" ) );

// Images
router.get( "/images", images.getAll );
router.get( "/images/:id", images.getByID );
router.delete( "/images/:id", images.delete );
router.post( "/images", images.add );

// Letter Pairs
router.get( "/pairs", pairs.allPairs );
router.get( "/pairs/random", pairs.randomPair );
router.get( "/pairs/:letterPair", pairs.getByLetterPair );

// Users
router.get( "/users", users.getAll );
router.get( "/users/:userID", users.getByID );

module.exports = router;
