const router = require( "express" ).Router();
const createError = require( "../utils/errors" );

// REST API
router.use( "/api", require( "./rest-api" ) );

// Home page
router.get( "/", ( req, res, next ) => {
	res.render( "home" );
} );

module.exports = router;
