const path = require( "path" );
const sassMiddleware = require( "../../node_modules/node-sass-middleware/middleware" );
const options = {
	src: path.join( __dirname, "../client" ),
	dest: path.join( __dirname, "../public" ),
	indentedSyntax: false, // true = .sass and false = .scss
	outputStyle: "compressed",
	sourceMap: true,
	debug: false
};
module.exports = sassMiddleware( options );
