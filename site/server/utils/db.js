const mysql = require( "mysql2" );
const myConnection = require( "./promise-myconnection" );
//const myConnection = require( 'express-myconnection' );

const connectionParams = {
	host: process.env.DB_HOST,
	port: process.env.DB_PORT,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_DATABASE
};

module.exports = myConnection( mysql, connectionParams, "single" );
