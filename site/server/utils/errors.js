const httpCreateError = require( "http-errors" );

module.exports = function createError ( code, err ) {
	if ( err === Object( err ) && err.sqlMessage ) {
		err = `MySQL ${err.code}: ${err.sqlMessage}`;
	}
	return httpCreateError( code, err );
};

