let mysql;
let dbConfig;
let connection; // This is used as a singleton in a single connection strategy
let pool;       // Pool singleton

/**
 * Handling connection disconnects, as defined here: https://github.com/felixge/node-mysql
 */
function handleDisconnect () {
	connection = mysql.createConnection( dbConfig );

	connection.connect( function ( err ) {
		if ( err ) {
			console.log( "error when connecting to db:", err );
			setTimeout( handleDisconnect, 2000 );
		}
	} );

	connection.on( "error", function ( err ) {
		console.log( "db error", err );
		if ( err.code === "PROTOCOL_CONNECTION_LOST" ) {
			handleDisconnect();
		} else {
			throw err;
		}
	} );
}

/**
 * Returns middleware that will handle mysql db connections
 *
 * @param engine
 * @param config
 * @param {String||undefined} strategy - default is single strategy
 * @return {Function}
 * @api public
 */
module.exports = function ( engine, config, strategy ) {

	if ( null === mysql ) {
		throw new Error( "Missing mysql module param!" );
	}
	if ( null === dbConfig ) {
		throw new Error( "Missing dbConfig module param!" );
	}
	if ( null === strategy ) {
		strategy = "single";
	}

	// Setting _mysql module ref
	mysql = engine;

	// Setting _dbConfig ref
	dbConfig = config;

	// Configuring strategies
	switch ( strategy ) {
		case "single":
			// Creating single connection instance
			connection = mysql.createConnection( dbConfig );
			handleDisconnect( dbConfig );
			break;
		case "pool":
			// Creating pool instance
			pool = mysql.createPool( dbConfig );
			break;
		case "request":
			// Nothing at this point do be done
			break;
		default:
			throw new Error( "Unsupported connection strategy!" );
	}

	return function ( req, res, next ) {
		const end = res.end;
		let poolConnection;
		let requestConnection;

		switch ( strategy ) {
			case "single":
				// getConnection will return singleton connection
				req.getConnection = function ( callback ) {
					callback( null, connection );
				};
				break;

			case "pool":
				// getConnection handled by mysql pool
				req.getConnection = function ( callback ) {
					// Returning cached connection from a pool, caching is on request level
					if ( poolConnection ) {
						return callback( null, poolConnection );
					}
					// Getting connection from a pool
					pool.getConnection( function ( err, connection ) {
						if ( err ) {
							return callback( err );
						}
						poolConnection = connection;
						callback( null, poolConnection );
					} );
				};
				break;

			case "request":
				// getConnection creates new connection per request
				req.getConnection = function ( callback ) {
					// Returning cached connection, caching is on request level
					if ( requestConnection ) {
						return callback( null, requestConnection );
					}
					// Creating new connection
					connection = mysql.createConnection( dbConfig );
					connection.connect( function ( err ) {
						if ( err ) {
							return callback( err );
						}
						requestConnection = connection;
						callback( null, requestConnection );
					} );
				};
				break;
		}

		res.end = function ( data, encoding ) {

			// Ending request connection if available
			if ( requestConnection ) {
				requestConnection.end();
			}

			// Releasing pool connection if available
			if ( poolConnection ) {
				poolConnection.release();
			}

			res.end = end;
			res.end( data, encoding );
		};

		next();
	};
};
